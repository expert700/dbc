cmake_minimum_required(VERSION 3.9)
project(dbc)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

file(GLOB_RECURSE SOURCE_FILES "src/*.cpp" "src/*.h")

#add_compile_options("/std:c++17")

add_executable(dbc ${SOURCE_FILES})