#pragma once

#include <string>
#include <utility>

class Token {
public:
    Token(std::string token, int depth): token(std::move(token)), depth(depth) {}
	Token(): token("null"), depth(0) {}

    std::string token;
    int depth;
};