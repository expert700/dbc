#include <csignal>
#include "Parser.h"

ASTNode* Parser::parse(std::vector<std::string> tIn) {
    std::vector<Token> tokens = findDepth(std::move(tIn));
    ASTNode* root = new ASTNode(ROOT, "root");
    parseRecursive(tokens, root);
    return root;
}

void Parser::parseRecursive(std::vector<Token> tokens, ASTNode* parent) {
	//TODO; Replace the bracket statement stuff with a more general function to grab the next normal / bracket statement.
	if (tokens.empty()) return;
	//std::cout << "Parsing: ";
	for (Token t : tokens) {
		//std::cout << t.token << " ";
	}
	//std::cout << std::endl;
	Token first = tokens.front();
	if (isControl(first.token)) {
		if (first.token == "prog") {
			ASTNode* programNode = new ASTNode(PROG, tokens.at(1).token);
			parent->addChild(programNode);
			parseRecursive(truncateStatement(tokens), programNode);
			parseRecursive(remainingStatements(tokens), parent);
		} else if (first.token == "for") {
			ASTNode* forNode = new ASTNode(FOR, "for");
			parent->addChild(forNode);
			tokens.erase(tokens.begin(), tokens.begin() + 2);
			std::vector<Token> statement;
            int endIndex = 0;
			for (int i = 0; i < tokens.size(); i++) {
				Token currToken = tokens.at(i);
				if (currToken.token == ";") {
					parseRecursive(statement, forNode);
					statement.clear();
				} else if (currToken.token == ")" && currToken.depth == first.depth) {
                    endIndex = i;
					break;
				} else {
					statement.push_back(currToken);
				}
			}
			parseRecursive(statement, forNode);
            tokens.erase(tokens.begin(), tokens.begin() + endIndex);

			ASTNode* forBlock = new ASTNode(STMT, "forblock");
			forNode->addChild(forBlock);
			parseRecursive(truncateStatement(tokens), forBlock);
			parseRecursive(remainingStatements(tokens), parent);
		} else if (first.token == "while") {
			ASTNode* whileNode = new ASTNode(WHILE, "while");
			parent->addChild(whileNode);
			tokens.erase(tokens.begin(), tokens.begin() + 2);
			std::vector<Token> statement;
			for (int i = 0; i < tokens.size(); i++) {
				Token currToken = tokens.at(i);
				if (currToken.token == ")" && currToken.depth == first.depth) {
					break;
				} else {
					statement.push_back(currToken);
				}
			}
			parseRecursive(statement, whileNode);

			ASTNode* whileBlock = new ASTNode(STMT, "whileblock");
			whileNode->addChild(whileBlock);
			parseRecursive(truncateStatement(tokens), whileBlock);
			parseRecursive(remainingStatements(tokens), parent);
		} else if (first.token == "if") {
			ASTNode* ifNode = new ASTNode(IF, "if");
			parent->addChild(ifNode);
			std::vector<Token> argTokens;
            int endIndex = 0;
			for (int i = 2; i < tokens.size(); i++) {
				Token currToken = tokens.at(i);
				if (currToken.token == ")" && currToken.depth == first.depth) {
                    endIndex = i;
                    break;
                }
				argTokens.push_back(currToken);
			}
			parseRecursive(argTokens, ifNode);
            tokens.erase(tokens.begin(), tokens.begin() + endIndex + 1);

			ASTNode* content = new ASTNode(STMT, "ifblock");
			ifNode->addChild(content);
			parseRecursive(truncateStatement(tokens), content);
			std::vector<Token> afterBrackets = remainingStatements(tokens);
			if (afterBrackets.size() > 0 && afterBrackets.front().token == "else") {
				parseRecursive(afterBrackets, ifNode);
			} else {
				parseRecursive(afterBrackets, parent);
			}
		} else if (first.token == "else") {
			if (tokens.at(1).token == "if") {
                ASTNode* elseIfNode = new ASTNode(ELIF, "elseif");
                parent->addChild(elseIfNode);
				std::vector<Token> argTokens;
				int endIndex = 0;
				for (int i = 3; i < tokens.size(); i++) {
					Token currToken = tokens.at(i);
					if (currToken.token == ")" && currToken.depth == first.depth) {
						endIndex = i;
						break;
					}
					argTokens.push_back(currToken);
				}
				parseRecursive(argTokens, elseIfNode);
				tokens.erase(tokens.begin(), tokens.begin() + endIndex + 1);

                ASTNode* content = new ASTNode(STMT, "elseifblock");
				elseIfNode->addChild(content);
				parseRecursive(truncateStatement(tokens), content);
            } else {
                ASTNode* elseNode = new ASTNode(STMT, "elseblock");
                parent->addChild(elseNode);
                parseRecursive(truncateStatement(tokens), elseNode);
            }
            std::vector<Token> remaining = remainingStatements(tokens);
            if (remaining.front().token == "else") {
                parseRecursive(remaining, parent);
            } else {
                parseRecursive(remaining, parent->getParent());
            }
		} else if (first.token == "return") {
			ASTNode* returnNode = new ASTNode(RET, first.token);
			parent->addChild(returnNode);
			tokens.erase(tokens.begin());
			parseRecursive(tokens, returnNode);
		}
	} else {
		//TODO; Less lazy way of identifying functions.
		if (isType(first.token) && tokens.size() > 2 && tokens.at(2).token == "(") {
			if (tokens.at(1).token == "main") {
				ASTNode* mainNode = new ASTNode(STMT, "main");
				parent->addChild(mainNode);
				parseRecursive(truncateStatement(tokens), mainNode);
				parseRecursive(remainingStatements(tokens), parent);
			}
		} else {
			if (tokens.size() == 1) {
				//TODO; Neaten.
				if (isType(first.token)) {
					ASTNode* typeNode = new ASTNode(TINF, first.token);
					parent->addChild(typeNode);
					return;
				}
				else if (isVariable(first.token)) {
					ASTNode* lkpNode = new ASTNode(LKP, first.token);
					parent->addChild(lkpNode);
					return;
				}

				std::shared_ptr<Variable> var = ASTNode::nullVarPtr;
				if (contains(first.token, stringLiteralChar)) {
					var = std::make_shared<Variable>(str, unescape(stripChar(first.token, '"')));
				} else if (contains(first.token, charLiteralChar)) {
					var = std::make_shared<Variable>(chr, unescape(stripChar(first.token, '\'')).front());
				} else if (isInteger(first.token)) {
                    var = std::make_shared<Variable>(i32, std::stoi(first.token));
                } else if (isLong(first.token)) {
                    first.token.erase(first.token.end() - 1);
                    var = std::make_shared<Variable>(i64, (long long) std::stol(first.token));
				} else if (isFloat(first.token)) {
                    var = std::make_shared<Variable>(f32, std::stof(first.token));
                } else if (isDouble(first.token)) {
                    first.token.erase(first.token.end() - 1);
                    var = std::make_shared<Variable>(f64, std::stod(first.token));
				} else if (isBoole(first.token)) {
					var = std::make_shared<Variable>(boole, toBoole(first.token));
				}
				std::string name = std::string("const") + std::to_string(getConstNumber());
				ASTNode::globalTable.addSymbol(name, var);
				ASTNode* lkpNode = new ASTNode(LKP, name);
				parent->addChild(lkpNode);
			} else if (tokens.size() == 2 && isType(first.token)) {
				ASTNode* declNode = new ASTNode(DECL, tokens.at(1).token);
				parent->addChild(declNode);
				tokens.erase(tokens.end() - 1);
				parseRecursive(tokens, declNode);
			} else if (hasMultipleStatements(tokens)) {
				std::vector<Token> firstStatement = truncateStatement(tokens);
				parseRecursive(firstStatement, parent);

				std::vector<Token> afterFirst = remainingStatements(tokens);
				parseRecursive(afterFirst, parent);
			} else if (isSingleStatement(tokens)) {
				tokens.pop_back();
				parseRecursive(tokens, parent);
			} else if (first.token == "(" && tokens.back().token == ")") {
                tokens.erase(tokens.begin());
                tokens.erase(tokens.end() - 1);
                parseRecursive(tokens, parent);
            } else {
				bool operatorOnMinDepth = false;
				int minDepth = INT32_MAX;
				for (Token t : tokens) {
					if (t.depth < minDepth) minDepth = t.depth;
				}

				for (Token t : tokens) {
					if (isOperator(t.token) && t.depth == minDepth) operatorOnMinDepth = true;
				}

				if (operatorOnMinDepth) {
					bool found = false;
					for (std::string opstr : operators) {
						for (int i = 0; i < tokens.size(); i++) {
							if (opstr == tokens.at(i).token) {
								ASTNode* opNode = new ASTNode(OP, tokens.at(i).token);
								parent->addChild(opNode);
								std::vector<Token> beforeOp;
								beforeOp = tokens;
								beforeOp.resize(i);
								std::vector<Token> afterOp;
								afterOp = tokens;
								afterOp.erase(afterOp.begin(), afterOp.begin() + i + 1);
								parseRecursive(beforeOp, opNode);
								parseRecursive(afterOp, opNode);
								found = true;
								break;
							}
						}
						if (found) break;
					}
				} else {
					ASTNode* callNode = new ASTNode(CALL, first.token);
					parent->addChild(callNode);
					std::vector<Token> argTokens;
					for (int i = 2; i < tokens.size(); i++) {
						Token currToken = tokens.at(i);
						if ((currToken.token == "," && currToken.depth == minDepth + 1) || (currToken.token == ")" && currToken.depth == minDepth)) {
							parseRecursive(argTokens, callNode);
							argTokens.clear();
						} else {
							argTokens.push_back(currToken);
						}
					}
				}
			}
		}
	}
}

std::vector<Token> Parser::findDepth(std::vector<std::string> tIn) {
    std::vector<Token> newTokens;
    int depth = 0;
    for (const std::string &s : tIn) {
        if (contains(s, ')') || contains(s, '}')) depth--;
        newTokens.emplace_back(s, depth);
        if (contains(s, '(') || contains(s, '{')) depth++;
    }
    return newTokens;
}

bool Parser::contains(std::string str, char c) {
    return std::find(str.begin(), str.end(), c) != str.end();
}

std::vector<Token> Parser::truncateStatement(std::vector<Token> tIn) {
    bool nextIsBracket = false;
    for (Token t : tIn) {
        if (t.token == ";" && !nextIsBracket) break;
        if (t.token == "{") {
            nextIsBracket = true;
            break;
        }
    }
    // The tokens in the statement.
    std::vector<Token> tokens;
    if (nextIsBracket) {
        bool inBracket = false;
        int startDepth = 0;
        for (Token t : tIn) {
            if (t.token == "{" && !inBracket) {
                inBracket = true;
                startDepth = t.depth;
            } else if (t.token == "}" && inBracket && t.depth == startDepth) {
                break;
            } else if (inBracket) {
                tokens.push_back(t);
            }
        }
    } else {
        for (Token t : tIn) {
            if (t.token == ";") break;
            tokens.push_back(t);
        }
    }
    return tokens;
}

std::vector<Token> Parser::remainingStatements(std::vector<Token> tIn) {
    bool nextIsBracket = false;
    for (Token t : tIn) {
        if (t.token == ";" && !nextIsBracket) break;
        if (t.token == "{") {
            nextIsBracket = true;
            break;
        }
    }

    std::vector<Token> tokens;
    if (nextIsBracket) {
        bool inBracket = false;
        int startDepth = 0;
        int endIndex = 0;
        for (int i = 0; i < tIn.size(); i++) {
            Token t = tIn.at(i);
            if (!inBracket && t.token == "{") {
                inBracket = true;
                startDepth = t.depth;
            } else if (inBracket && t.token == "}" && t.depth == startDepth) {
                endIndex = i;
                break;
            }
        }
        for (int i = endIndex + 1; i < tIn.size(); i++) {
            tokens.push_back(tIn.at(i));
        }
    } else {
        int endIndex = 0;
        for (int i = 0; i < tIn.size(); i++) {
            if (tIn.at(i).token == ";") {
                endIndex = i;
                break;
            }
        }
        for (int i = endIndex + 1; i < tIn.size(); i++) {
            tokens.push_back(tIn.at(i));
        }
    }
    return tokens;
}

bool Parser::isInteger(std::string string) {
    for (char c : string) {
        if (!std::isdigit(c)) return false;
    }
    return true;
}

bool Parser::isLong(std::string string) {
	for (int i = 0; i < string.size(); i++) {
		if (!std::isdigit(string.at(i))) {
            if (i != string.size() - 1 || string.at(i) != 'l') return false;
        }
	}
    return true;
}

bool Parser::isFloat(std::string string) {
    for (char c : string) {
        if(!std::isdigit(c) && c != '.') return false;
    }
    return true;
}

bool Parser::isDouble(std::string string) {
    for (int i = 0; i < string.size(); i++) {
        if (!std::isdigit(string.at(i)) && string.at(i) != '.') {
            if (i != string.size() - 1 || string.at(i) != 'd') return false;
        }
    }
    return true;
}

bool Parser::isType(std::string string) {
    return std::find(types.begin(), types.end(), string) != types.end();
}

Token Parser::findFirstType(std::vector<Token> tIn) {
    for (Token t : tIn) {
        if (std::find(types.begin(), types.end(), t.token) != types.end()) return t;
    }
    return Token(std::string(), -1);
}

int Parser::getConstNumber() {
    static int count = 0;
    return count++;
}

bool Parser::isVariable(std::string string) {
    if (string == "true" || string == "false") return false;
    bool notNum = !isInteger(string);
    notNum = notNum && !isFloat(string);
    notNum = notNum && !isLong(string);
    notNum = notNum && !isDouble(string);
    std::locale loc;
    std::string::size_type i = 0;
    while (i < string.length() && std::isalnum(string[i])) {
        i++;
    }
    return i == string.length() && notNum;
}

bool Parser::isBoole(std::string string) {
    return string == "true" || string == "false";
}

bool Parser::toBoole(std::string string) {
    return string == "true";
}

bool Parser::hasMultipleStatements(std::vector<Token> tokens) {
    for (int i = 0; i < tokens.size(); i++) {
        if (tokens.at(i).token == ";" && tokens.size() != i + 1) return true;
    }
    return false;
}

bool Parser::isSingleStatement(std::vector<Token> tokens) {
	if (tokens.back().token == ";") return true;
	return false;
}

std::string Parser::stripChar(std::string string, char c) {
    std::string output;
    for (char i : string) {
        if (i != c) output += i;
    }
    return output;
}

bool Parser::isOperator(std::string string) {
    return std::find(operators.begin(), operators.end(), string) != operators.end();
}

bool Parser::isControl(std::string string) {
    return std::find(controls.begin(), controls.end(), string) != controls.end();
}


std::string Parser::unescape(std::string string) {
    std::string output;
    for (int i = 0; i < string.length(); i++) {
        char c = string.at(i);
        if (c == '\\' && i != string.length() - 1) {
            char next = string.at(i + 1);
            switch (next) {
                case '\\':
                    output += '\\';
                    break;
                case 'n':
                    output += '\n';
                    break;
                default:
                    break;
            }
            i++;
        } else {
            output += c;
        }
    }
    return output;
}
