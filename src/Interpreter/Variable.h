#pragma once

#include <string>
#include <any>
#include <memory>
#include <iostream>
#include <cmath>
#include <ctgmath>

enum VariableType {
    boole,
    chr,
    i32,
    i64,
    f32,
    f64,
    str,
    null
};

class Variable {
public:
    Variable(VariableType type, bool b);
    Variable(VariableType type, char c);
    Variable(VariableType type, int i);
    Variable(VariableType type, long long l);
    Variable(VariableType type, float f);
    Variable(VariableType type, double d);
    Variable(VariableType type, std::string s);
    Variable(VariableType type);
    Variable(const Variable& other);

    std::any getValue();
	void setValue(std::any value);
    VariableType getType();
    std::string toString();
	Variable add(const Variable& other);
	Variable sub(const Variable& other);
	Variable mul(const Variable& other);
	Variable div(const Variable& other);
	Variable mod(const Variable& other);
	Variable equals(const Variable& other);
	Variable notEquals(const Variable& other);
	Variable greater(const Variable& other);
	Variable less(const Variable& other);

    static VariableType resolveTypeString(std::string string);
	static std::string resolveTypeEnum(VariableType type);
private:
    VariableType type;
    std::any value;
};
