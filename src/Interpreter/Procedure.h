#pragma once

#include "Variable.h"

// Wonky fix to a circular dependency.
class ASTNode;

class Procedure {
public:
    Procedure(VariableType returnType, ASTNode* rootNode);

    VariableType  returnType;
    ASTNode* rootNode;
};
