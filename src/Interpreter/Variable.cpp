#include "Variable.h"
#include <utility>

Variable::Variable(VariableType type, bool b): type(type), value(b) {}

Variable::Variable(VariableType type, char c): type(type), value(c) {}

Variable::Variable(VariableType type, int i): type(type), value(i) {}

Variable::Variable(VariableType type, long long l): type(type), value(l) {}

Variable::Variable(VariableType type, float f): type(type), value(f) {}

Variable::Variable(VariableType type, double d): type(type), value(d) {}

Variable::Variable(VariableType type, std::string s): type(type), value(s) {}

Variable::Variable(VariableType type): type(type) {
    switch (type) {
        case boole:
            value = false;
            break;
        case chr:
            value = (char) 0x0;
            break;
        case i32:
            value = 0;
            break;
        case i64:
            value = 0ll;
            break;
        case f32:
            value = 0.0f;
            break;
        case f64:
            value = 0.0l;
            break;
        case str:
            value = std::string();
            break;
        default:
            value = nullptr;
            break;
    }
}

Variable::Variable(const Variable &other): type(other.type), value(other.value) {}

std::any Variable::getValue() {
    return value;
}

void Variable::setValue(std::any value) {
	this->value = value;
}

VariableType Variable::getType() {
    return type;
}

VariableType Variable::resolveTypeString(std::string string) {
    if (string == "boole") {
        return boole;
    } else if (string == "chr") {
        return chr;
    } else if (string == "i32") {
        return i32;
    } else if (string == "i64") {
        return i64;
    } else if (string == "f32") {
        return f32;
    } else if (string == "f64") {
        return f64;
    } else if (string == "str") {
        return str;
    } else {
        return null;
    }
}

std::string Variable::resolveTypeEnum(VariableType type) {
	switch (type) {
		case boole:
			return "bool";
		case chr:
			return "char";
		case i32:
			return "i32";
		case i64:
			return "i64";
		case f32:
			return "f32";
		case f64:
			return "f64";
		case str:
			return "str";
		default:
			return "null";
	}
}

std::string Variable::toString() {
    switch (type) {
        case boole:
            return std::any_cast<bool>(value) ? std::string("true") : std::string("false");
        case chr:
            return std::string() + std::any_cast<char>(value);
        case i32:
            return std::to_string(std::any_cast<int>(value));
        case i64:
            return std::to_string(std::any_cast<long long>(value));
        case f32:
            return std::to_string(std::any_cast<float>(value));
        case f64:
            return std::to_string(std::any_cast<double>(value));
        case str:
            return std::any_cast<std::string>(value);
        default:
            return std::string("null");
    }
}

Variable Variable::add(const Variable& other) {
	if (type == boole || type == null || other.type == boole || other.type == null) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(chr, std::any_cast<char>(value) + std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(i32, std::any_cast<char>(value) + std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<char>(value) + std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(f32, std::any_cast<char>(value) + std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(f64, std::any_cast<char>(value) + std::any_cast<double>(other.value));
	} else if (type == chr && other.type == str) {
		return Variable(str, std::any_cast<char>(value) + std::any_cast<std::string>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(i32, std::any_cast<int>(value) + std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(i32, std::any_cast<int>(value) + std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<int>(value) + std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(f32, std::any_cast<int>(value) + std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(f64, std::any_cast<int>(value) + std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == str) {
		return Variable(str, std::to_string(std::any_cast<int>(value)) + std::any_cast<std::string>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(i64, (long long) std::any_cast<long long>(value) + std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(i64, (long long) std::any_cast<long long>(value) + std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<long long>(value) + std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(f32, std::any_cast<long long>(value) + std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(f64, std::any_cast<long long>(value) + std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == str) {
		return Variable(str, std::to_string(std::any_cast<long long>(value)) + std::any_cast<std::string>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(f32, std::any_cast<float>(value) + std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(f32, std::any_cast<float>(value) + std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(f32, std::any_cast<float>(value) + std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(f32, std::any_cast<float>(value) + std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(f64, std::any_cast<float>(value) + std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == str) {
		return Variable(str, std::to_string(std::any_cast<float>(value)) + std::any_cast<std::string>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(f64, std::any_cast<double>(value) + std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(f64, std::any_cast<double>(value) + std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(f64, std::any_cast<double>(value) + std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(f64, std::any_cast<double>(value) + std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(f64, std::any_cast<double>(value) + std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == str) {
		return Variable(str, std::to_string(std::any_cast<double>(value)) + std::any_cast<std::string>(other.value));
	} else if (type == str && other.type == chr) {
		return Variable(str, std::any_cast<std::string>(value) + std::any_cast<char>(other.value));
	} else if (type == str && other.type == i32) {
		return Variable(str, std::any_cast<std::string>(value) + std::to_string(std::any_cast<int>(other.value)));
	} else if (type == str && other.type == i64) {
		return Variable(str, std::any_cast<std::string>(value) + std::to_string(std::any_cast<long long>(other.value)));
	} else if (type == str && other.type == f32) {
		return Variable(str, std::any_cast<std::string>(value) + std::to_string(std::any_cast<float>(other.value)));
	} else if (type == str && other.type == f64) {
		return Variable(str, std::any_cast<std::string>(value) + std::to_string(std::any_cast<double>(other.value)));
	} else if (type == str && other.type == str) {
		return Variable(str, std::any_cast<std::string>(value) + std::any_cast<std::string>(other.value));
	}
	return Variable(null);
}

Variable Variable::sub(const Variable& other) {
	if (type == boole || type == null || other.type == boole || other.type == null || type == str || other.type == str) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(chr, std::any_cast<char>(value) - std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(i32, std::any_cast<char>(value) - std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<char>(value) - std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(f32, std::any_cast<char>(value) - std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(f64, std::any_cast<char>(value) - std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(i32, std::any_cast<int>(value) - std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(i32, std::any_cast<int>(value) - std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<int>(value) - std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(f32, std::any_cast<int>(value) - std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(f64, std::any_cast<int>(value) - std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(i64, (long long) std::any_cast<long long>(value) - std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(i64, (long long) std::any_cast<long long>(value) - std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<long long>(value) - std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(f32, std::any_cast<long long>(value) - std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(f64, std::any_cast<long long>(value) - std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(f32, std::any_cast<float>(value) - std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(f32, std::any_cast<float>(value) - std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(f32, std::any_cast<float>(value) - std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(f32, std::any_cast<float>(value) - std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(f64, std::any_cast<float>(value) - std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(f64, std::any_cast<double>(value) - std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(f64, std::any_cast<double>(value) - std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(f64, std::any_cast<double>(value) - std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(f64, std::any_cast<double>(value) - std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(f64, std::any_cast<double>(value) - std::any_cast<double>(other.value));
	}
	return Variable(null);
}

Variable Variable::mul(const Variable& other) {
	if (type == boole || type == null || other.type == boole || other.type == null || type == str || other.type == str) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(chr, std::any_cast<char>(value) * std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(i32, std::any_cast<char>(value) * std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<char>(value) * std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(f32, std::any_cast<char>(value) * std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(f64, std::any_cast<char>(value) * std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(i32, std::any_cast<int>(value) * std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(i32, std::any_cast<int>(value) * std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<int>(value) * std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(f32, std::any_cast<int>(value) * std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(f64, std::any_cast<int>(value) * std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(i64, (long long) std::any_cast<long long>(value) * std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(i64, (long long) std::any_cast<long long>(value) * std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<long long>(value) * std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(f32, std::any_cast<long long>(value) * std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(f64, std::any_cast<long long>(value) * std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(f32, std::any_cast<float>(value) * std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(f32, std::any_cast<float>(value) * std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(f32, std::any_cast<float>(value) * std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(f32, std::any_cast<float>(value) * std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(f64, std::any_cast<float>(value) * std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(f64, std::any_cast<double>(value) * std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(f64, std::any_cast<double>(value) * std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(f64, std::any_cast<double>(value) * std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(f64, std::any_cast<double>(value) * std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(f64, std::any_cast<double>(value) * std::any_cast<double>(other.value));
	}
	return Variable(null);
}

Variable Variable::div(const Variable& other) {
	if (type == boole || type == null || other.type == boole || other.type == null || type == str || other.type == str) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(chr, std::any_cast<char>(value) / std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(i32, std::any_cast<char>(value) / std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<char>(value) / std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(f32, std::any_cast<char>(value) / std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(f64, std::any_cast<char>(value) / std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(i32, std::any_cast<int>(value) / std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(i32, std::any_cast<int>(value) / std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<int>(value) / std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(f32, std::any_cast<int>(value) / std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(f64, std::any_cast<int>(value) / std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(i64, (long long) std::any_cast<long long>(value) / std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(i64, (long long) std::any_cast<long long>(value) / std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<long long>(value) / std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(f32, std::any_cast<long long>(value) / std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(f64, std::any_cast<long long>(value) / std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(f32, std::any_cast<float>(value) / std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(f32, std::any_cast<float>(value) / std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(f32, std::any_cast<float>(value) / std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(f32, std::any_cast<float>(value) / std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(f64, std::any_cast<float>(value) / std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(f64, std::any_cast<double>(value) / std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(f64, std::any_cast<double>(value) / std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(f64, std::any_cast<double>(value) / std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(f64, std::any_cast<double>(value) / std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(f64, std::any_cast<double>(value) / std::any_cast<double>(other.value));
	}
	return Variable(null);
}

Variable Variable::mod(const Variable& other) {
	if (type == boole || type == null || other.type == boole || other.type == null || type == str || other.type == str) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(chr, std::any_cast<char>(value) % std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(i32, std::any_cast<char>(value) % std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<char>(value) % std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(f32, std::fmod(std::any_cast<char>(value), std::any_cast<float>(other.value)));
	} else if (type == chr && other.type == f64) {
		return Variable(f32, std::fmod(std::any_cast<char>(value), std::any_cast<double>(other.value)));
	} else if (type == i32 && other.type == chr) {
		return Variable(i32, std::any_cast<int>(value) % std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(i32, std::any_cast<int>(value) % std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<int>(value) % std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(f32, std::fmod(std::any_cast<int>(value), std::any_cast<float > (other.value)));
	} else if (type == i32 && other.type == f64) {
		return Variable(f32, std::fmod(std::any_cast<int>(value), std::any_cast<double>(other.value)));
	} else if (type == i64 && other.type == chr) {
		return Variable(i64, (long long) std::any_cast<long long>(value) % std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(i64, (long long) std::any_cast<long long>(value) % std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(i64, (long long) std::any_cast<long long>(value) % std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(f32, std::fmod(std::any_cast<long long>(value), std::any_cast<float>(other.value)));
	} else if (type == i64 && other.type == f64) {
		return Variable(f32, std::fmod(std::any_cast<long long>(value), std::any_cast<double>(other.value)));
	} else if (type == f32 && other.type == chr) {
		return Variable(f32, std::fmod(std::any_cast<float>(value), std::any_cast<char>(other.value)));
	} else if (type == f32 && other.type == i32) {
		return Variable(f32, std::fmod(std::any_cast<float>(value), std::any_cast<int>(other.value)));
	} else if (type == f32 && other.type == i64) {
		return Variable(f32, std::fmod(std::any_cast<float>(value), std::any_cast<long long>(other.value)));
	} else if (type == f32 && other.type == f32) {
		return Variable(f32, std::fmod(std::any_cast<float>(value), std::any_cast<float>(other.value)));
	} else if (type == f32 && other.type == f64) {
		return Variable(f32, std::fmod(std::any_cast<float>(value), std::any_cast<double>(other.value)));
	} else if (type == f64 && other.type == chr) {
		return Variable(f32, std::fmod(std::any_cast<double>(value), std::any_cast<char>(other.value)));
	} else if (type == f64 && other.type == i32) {
		return Variable(f32, std::fmod(std::any_cast<double>(value), std::any_cast<int>(other.value)));
	} else if (type == f64 && other.type == i64) {
		return Variable(f32, std::fmod(std::any_cast<double>(value), std::any_cast<long long>(other.value)));
	} else if (type == f64 && other.type == f32) {
		return Variable(f32, std::fmod(std::any_cast<double>(value), std::any_cast<float>(other.value)));
	} else if (type == f64 && other.type == f64) {
		return Variable(f32, std::fmod(std::any_cast<double>(value), std::any_cast<double>(other.value)));
	}
	return Variable(null);
}

Variable Variable::equals(const Variable& other) {
	if (type == null && other.type == null) return Variable(boole, true);
	else if (type == boole && other.type == boole) {
		bool a = std::any_cast<bool>(value);
		bool b = std::any_cast<bool>(other.value);
		return Variable(boole, a == b);
	}

	if (type == chr && other.type == chr) {
		return Variable(boole, std::any_cast<char>(value) == std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(boole, std::any_cast<char>(value) == std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(boole, std::any_cast<char>(value) == std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(boole, std::any_cast<char>(value) == std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(boole, std::any_cast<char>(value) == std::any_cast<double>(other.value));
	} else if (type == chr && other.type == str) {
		return Variable(boole, std::string() + std::any_cast<char>(value) == std::any_cast<std::string>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(boole, std::any_cast<int>(value) == std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(boole, std::any_cast<int>(value) == std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(boole, std::any_cast<int>(value) == std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(boole, std::any_cast<int>(value) == std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(boole, std::any_cast<int>(value) == std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == str) {
		return Variable(boole, std::to_string(std::any_cast<int>(value)) == std::any_cast<std::string>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(boole, std::any_cast<long long>(value) == std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(boole, std::any_cast<long long>(value) == std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(boole, std::any_cast<long long>(value) == std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(boole, std::any_cast<long long>(value) == std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(boole, std::any_cast<long long>(value) == std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == str) {
		return Variable(boole, std::to_string(std::any_cast<long long>(value)) == std::any_cast<std::string>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(boole, std::any_cast<float>(value) == std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(boole, std::any_cast<float>(value) == std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(boole, std::any_cast<float>(value) == std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(boole, std::any_cast<float>(value) == std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(boole, std::any_cast<float>(value) == std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == str) {
		return Variable(boole, std::to_string(std::any_cast<float>(value)) == std::any_cast<std::string>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(boole, std::any_cast<double>(value) == std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(boole, std::any_cast<double>(value) == std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(boole, std::any_cast<double>(value) == std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(boole, std::any_cast<double>(value) == std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(boole, std::any_cast<double>(value) == std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == str) {
		return Variable(boole, std::to_string(std::any_cast<double>(value)) == std::any_cast<std::string>(other.value));
	} else if (type == str && other.type == chr) {
		return Variable(boole, std::any_cast<std::string>(value) == std::string() + std::any_cast<char>(other.value));
	} else if (type == str && other.type == i32) {
		return Variable(boole, std::any_cast<std::string>(value) == std::to_string(std::any_cast<int>(other.value)));
	} else if (type == str && other.type == i64) {
		return Variable(boole, std::any_cast<std::string>(value) == std::to_string(std::any_cast<long long>(other.value)));
	} else if (type == str && other.type == f32) {
		return Variable(boole, std::any_cast<std::string>(value) == std::to_string(std::any_cast<float>(other.value)));
	} else if (type == str && other.type == f64) {
		return Variable(boole, std::any_cast<std::string>(value) == std::to_string(std::any_cast<double>(other.value)));
	} else if (type == str && other.type == str) {
		return Variable(boole, std::any_cast<std::string>(value) == std::any_cast<std::string>(other.value));
	}
	return Variable(boole, false);
}

Variable Variable::notEquals(const Variable& other) {
	Variable equal = equals(other);
	bool val = std::any_cast<bool>(equal.value);
	return Variable(boole, !val);
}

Variable Variable::greater(const Variable& other) {
	if (type == boole || type == str || type == null || other.type == boole || other.type == str || other.type == null) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(boole, std::any_cast<char>(value) > std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(boole, std::any_cast<char>(value) > std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(boole, std::any_cast<char>(value) > std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(boole, std::any_cast<char>(value) > std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(boole, std::any_cast<char>(value) > std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(boole, std::any_cast<int>(value) > std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(boole, std::any_cast<int>(value) > std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(boole, std::any_cast<int>(value) > std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(boole, std::any_cast<int>(value) > std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(boole, std::any_cast<int>(value) > std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(boole, std::any_cast<long long>(value) > std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(boole, std::any_cast<long long>(value) > std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(boole, std::any_cast<long long>(value) > std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(boole, std::any_cast<long long>(value) > std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(boole, std::any_cast<long long>(value) > std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(boole, std::any_cast<float>(value) > std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(boole, std::any_cast<float>(value) > std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(boole, std::any_cast<float>(value) > std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(boole, std::any_cast<float>(value) > std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(boole, std::any_cast<float>(value) > std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(boole, std::any_cast<double>(value) > std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(boole, std::any_cast<double>(value) > std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(boole, std::any_cast<double>(value) > std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(boole, std::any_cast<double>(value) > std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(boole, std::any_cast<double>(value) > std::any_cast<double>(other.value));
	}
	return Variable(null);
}

Variable Variable::less(const Variable& other) {
	if (type == boole || type == str || type == null || other.type == boole || other.type == str || other.type == null) return Variable(null);

	if (type == chr && other.type == chr) {
		return Variable(boole, std::any_cast<char>(value) < std::any_cast<char>(other.value));
	} else if (type == chr && other.type == i32) {
		return Variable(boole, std::any_cast<char>(value) < std::any_cast<int>(other.value));
	} else if (type == chr && other.type == i64) {
		return Variable(boole, std::any_cast<char>(value) < std::any_cast<long long>(other.value));
	} else if (type == chr && other.type == f32) {
		return Variable(boole, std::any_cast<char>(value) < std::any_cast<float>(other.value));
	} else if (type == chr && other.type == f64) {
		return Variable(boole, std::any_cast<char>(value) < std::any_cast<double>(other.value));
	} else if (type == i32 && other.type == chr) {
		return Variable(boole, std::any_cast<int>(value) < std::any_cast<char>(other.value));
	} else if (type == i32 && other.type == i32) {
		return Variable(boole, std::any_cast<int>(value) < std::any_cast<int>(other.value));
	} else if (type == i32 && other.type == i64) {
		return Variable(boole, std::any_cast<int>(value) < std::any_cast<long long>(other.value));
	} else if (type == i32 && other.type == f32) {
		return Variable(boole, std::any_cast<int>(value) < std::any_cast<float>(other.value));
	} else if (type == i32 && other.type == f64) {
		return Variable(boole, std::any_cast<int>(value) < std::any_cast<double>(other.value));
	} else if (type == i64 && other.type == chr) {
		return Variable(boole, std::any_cast<long long>(value) < std::any_cast<char>(other.value));
	} else if (type == i64 && other.type == i32) {
		return Variable(boole, std::any_cast<long long>(value) < std::any_cast<int>(other.value));
	} else if (type == i64 && other.type == i64) {
		return Variable(boole, std::any_cast<long long>(value) < std::any_cast<long long>(other.value));
	} else if (type == i64 && other.type == f32) {
		return Variable(boole, std::any_cast<long long>(value) < std::any_cast<float>(other.value));
	} else if (type == i64 && other.type == f64) {
		return Variable(boole, std::any_cast<long long>(value) < std::any_cast<double>(other.value));
	} else if (type == f32 && other.type == chr) {
		return Variable(boole, std::any_cast<float>(value) < std::any_cast<char>(other.value));
	} else if (type == f32 && other.type == i32) {
		return Variable(boole, std::any_cast<float>(value) < std::any_cast<int>(other.value));
	} else if (type == f32 && other.type == i64) {
		return Variable(boole, std::any_cast<float>(value) < std::any_cast<long long>(other.value));
	} else if (type == f32 && other.type == f32) {
		return Variable(boole, std::any_cast<float>(value) < std::any_cast<float>(other.value));
	} else if (type == f32 && other.type == f64) {
		return Variable(boole, std::any_cast<float>(value) < std::any_cast<double>(other.value));
	} else if (type == f64 && other.type == chr) {
		return Variable(boole, std::any_cast<double>(value) < std::any_cast<char>(other.value));
	} else if (type == f64 && other.type == i32) {
		return Variable(boole, std::any_cast<double>(value) < std::any_cast<int>(other.value));
	} else if (type == f64 && other.type == i64) {
		return Variable(boole, std::any_cast<double>(value) < std::any_cast<long long>(other.value));
	} else if (type == f64 && other.type == f32) {
		return Variable(boole, std::any_cast<double>(value) < std::any_cast<float>(other.value));
	} else if (type == f64 && other.type == f64) {
		return Variable(boole, std::any_cast<double>(value) < std::any_cast<double>(other.value));
	}
	return Variable(null);
}