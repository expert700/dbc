#include "Procedure.h"

#include <utility>

#include "ASTNode.h"

Procedure::Procedure(VariableType returnType, ASTNode* rootNode): returnType(returnType), rootNode(rootNode) {}