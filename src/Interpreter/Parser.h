#pragma once

#include <algorithm>
#include <utility>
#include <iostream>
#include <cctype>

#include "ASTNode.h"
#include "Token.h"
#include "Definitions.h"

class Parser {
public:
    ASTNode* parse(std::vector<std::string> tIn);

private:
    std::vector<Token> findDepth(std::vector<std::string> tIn);
    void parseRecursive(std::vector<Token> tokens, ASTNode* parent);
    int getConstNumber();
    bool contains(std::string str, char c);
    std::vector<Token> truncateStatement(std::vector<Token> tIn);
    std::vector<Token> remainingStatements(std::vector<Token> tIn);
    bool isInteger(std::string string);
	bool isLong(std::string string);
    bool isFloat(std::string string);
	bool isDouble(std::string string);
    bool isType(std::string string);
    bool isVariable(std::string string);
    bool isBoole(std::string string);
    bool isOperator(std::string string);
    bool isControl(std::string string);
    bool toBoole(std::string string);
    bool hasMultipleStatements(std::vector<Token> tokens);
	bool isSingleStatement(std::vector<Token> tokens);
    Token findFirstType(std::vector<Token> tIn);
    std::string stripChar(std::string string, char c);
    std::string unescape(std::string string);
};