#include "SymbolTable.h"
#include <utility>
#include <map>

void SymbolTable::addSymbol(std::string name, std::shared_ptr<Variable> var) {
	symbols.insert(std::pair<std::string, std::shared_ptr<Variable>>(name, var));
}

void SymbolTable::addSymbol(std::string name, std::shared_ptr<Procedure> proc) {
	symbols.insert(std::pair<std::string, std::shared_ptr<Procedure>>(name, proc));
}

void SymbolTable::removeSymbol(std::string name) {
    //TODO; This will break if there are multiple symbols with the same name.
    symbols.erase(name);
}

void SymbolTable::deleteSymbols() {
    symbols.clear();
}

std::any SymbolTable::getSymbol(std::string name) {
	return symbols.find(name)->second;
}

bool SymbolTable::hasSymbol(std::string name) {
	return symbols.find(name) != symbols.end();
}
