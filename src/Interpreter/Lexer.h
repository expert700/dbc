#pragma once

#include <algorithm>
#include <iostream>

#include "Definitions.h"

class Lexer {
public:
    Lexer();
    
    std::vector<std::string> lex(std::vector<std::string> lines);
    
private:
    bool isSplitChar(char c);
    bool isOperator(std::string s);
    bool isWhitespace(char c);
    bool isAllWhitespace(std::string s);
};
