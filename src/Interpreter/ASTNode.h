#pragma once

#include <vector>
#include <string>
#include <memory>
#include <algorithm>

#include "Variable.h"
#include "SymbolTable.h"

enum ASTNodeType {
    ROOT,
    PROG,
    STMT,
	WHILE,
	FOR,
	IF,
	ELIF,
    CALL,
    LKP, // Variable lookup
    DECL,
    TINF, // Type info
    DESTR,
    OP,
    RET
};

class ASTNodeCleaner {
public:
	ASTNodeCleaner(ASTNode* target);
	~ASTNodeCleaner();
private:
	ASTNode* target;
};

class ASTNode {
public:
    ASTNode(ASTNodeType type, std::string name);
    ASTNode(const ASTNode& other);
    ~ASTNode();

    std::shared_ptr<Variable> eval();
    void addChild(ASTNode* node);
    std::vector<ASTNode*> getChildren();
    void printRecursive(int depth = 1, std::vector<int> continues = {});
	ASTNode* getParent();
    std::any getSymbol(std::string symbol);
    void cleanLocalTable();
    ASTNode* getScope();

    static SymbolTable globalTable;
	static const std::shared_ptr<Variable> nullVarPtr;

    ASTNodeType type;
private:
    std::string name;
    std::vector<ASTNode*> children;
	ASTNode* parent;
    SymbolTable localTable;
};
