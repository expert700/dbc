#include "Lexer.h"

Lexer::Lexer() {}

std::vector<std::string> Lexer::lex(std::vector<std::string> lines) {
    std::vector<std::string> tokens;
    for (std::string line : lines) {
        std::string token;
        bool matching = false;
        for (int i = 0; i < line.size(); i++) {
            char c = line.at(i);
            if (isWhitespace(c) && !isAllWhitespace(token) && !matching) {
                tokens.push_back(token);
                token = "";
            } else if (line.size() != i + 1 && isOperator(std::string() + c + line.at(i + 1)) && !matching) {
                if (!isAllWhitespace(token)) tokens.push_back(token);
                token = "";
                std::string op;
                op += c;
                op += line.at(i + 1);
                tokens.push_back(op);
                i++;
            } else if (isSplitChar(c) && !matching) {
                if (!isAllWhitespace(token)) tokens.push_back(token);
                token = "";
                std::string sc;
                sc += c;
                tokens.push_back(sc);
            } else if (!isWhitespace(c) || matching) {
                if (c == stringLiteralChar) {
                    matching = !matching;
                }
                token += c;
            }
        }
        if (!isAllWhitespace(token)) tokens.push_back(token);
    }
    return tokens;
}

bool Lexer::isSplitChar(char c) {
    std::string str;
    str += c;
    bool shouldSplit = false;
    if (std::find(miscSplits.begin(), miscSplits.end(), str) != miscSplits.end()) shouldSplit = true;
    else if (c == lineEndCharacter) shouldSplit = true;
    return shouldSplit || isOperator(str);
}

bool Lexer::isOperator(std::string s) {
    return std::find(operators.begin(), operators.end(), s) != operators.end();
}


bool Lexer::isWhitespace(char c) {
    return std::find(whitespace.begin(), whitespace.end(), c) != whitespace.end();
}


bool Lexer::isAllWhitespace(std::string s) {
    for (char c : s) {
        if (std::find(whitespace.begin(), whitespace.end(), c) == whitespace.end()) return false;
    }
    
    return true;
}


