#pragma once

#include <any>
#include <unordered_map>
#include <vector>
#include <memory>

#include "Procedure.h"

//TODO; Rework symbol table. The map is the slowest part of B-- right now.
class SymbolTable {
public:
	void addSymbol(std::string name, std::shared_ptr<Variable> var);
	void addSymbol(std::string name, std::shared_ptr<Procedure> proc);
    void removeSymbol(std::string name);
    void deleteSymbols();

    std::any getSymbol(std::string name);
	bool hasSymbol(std::string name);
private:
    std::unordered_map<std::string, std::any> symbols;
};