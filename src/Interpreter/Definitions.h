#pragma once

#include <vector>
#include <string>
#include <map>

const char lineEndCharacter = ';';
const char stringLiteralChar = '"';
const char charLiteralChar = '\'';

const std::vector<std::string> operators = {"&&", "||", "==", "!=", "!", ">", "<", "=", "-", "+", "/", "%", "*" };

const std::vector<std::string> miscSplits = {"(", ")", "{", "}", ",", ":"};

const std::vector<std::string> types = {"i32", "i64", "f32", "f64", "string", "char", "bool", "null"};

const std::vector<std::string> controls = {"prog", "for", "while", "if", "else", "return"};

const std::vector<char> whitespace = {0x00, 0x01, 0x02, 0x03, 0x04, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x20};
