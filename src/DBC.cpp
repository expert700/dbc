#include <iostream>
#include <fstream>
#include <vector>
#include <cstdint>

#ifdef WIN32 
#include <Windows.h>
#elif UNIX
#include <sys/ioctl.h>
#endif WIN32


#include "Interpreter/Lexer.h"
#include "Interpreter/Variable.h"
#include "Interpreter/Parser.h"
#include "Interpreter/SymbolTable.h"

int main(int argc, char **argv) {
	//NOTE; You might be encountering errors because you're not running in the project directory you retard.
    std::ifstream fileIn("example/fizzbuzz.bmm");
    std::string line;
    std::vector<std::string> lines;
    while (std::getline(fileIn, line)) {
        if (!line.empty()) lines.push_back(line);
    }
    Lexer lexer;
    std::vector<std::string> tokens = lexer.lex(lines);

    Parser parser;
    ASTNode* root = parser.parse(tokens);

    
	// This is all just to print a perfectly formatted line.
	int columns = 0;
#ifdef WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
#elif UNIX
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	columns = w.ws_col;
#endif WIN32
	std::string structureDesc("Program Structure");
	std::string execBeginDesc("Begin Execution");

	int columnsa = columns - structureDesc.length();
	for (int i = 0; i < columnsa / 2; i++) std::cout << "=";
	std::cout << structureDesc;
	for (int i = 0; i < columnsa / 2 + columnsa % 2; i++) std::cout << "=";
	std::cout << "\n";

	root->printRecursive();

	int columnsb = columns - execBeginDesc.length();
	std::cout << "\n";
	for (int i = 0; i < columnsb / 2; i++) std::cout << "=";
	std::cout << execBeginDesc;
	for (int i = 0; i < columnsb / 2 + columnsb % 2; i++) std::cout << "=";
	std::cout << "\n";

	int y;
	std::cin >> y;
	std::cin.clear();
	std::cin.ignore(10000, '\n');

	// Very important to keep this. Without buffered output, frequentg print statements will tank the program.
	char buffer[4096];
	setvbuf(stdout, buffer, _IOFBF, sizeof(buffer));
    root->eval();
	delete root;

	std::cin >> y;
	std::cin >> y;
	int x;
	std::cin >> x;
    return 0;
}

